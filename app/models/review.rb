class Review < ApplicationRecord
  belongs_to :user
  belongs_to :book

  def calculate_average_rating
  	self.average_rating = ((self.content_rating.to_f + self.recommand_rating.to_f)/2).round(1)
  end
end
