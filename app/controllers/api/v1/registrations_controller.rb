class Api::V1::RegistrationsController < Devise::RegistrationsController
	before_action :ensure_params_exist, only: :create
	#sign_up
	def create
		user = User.new user_params
		if user.save
			json_response "Successfully Signed Up", true, {user: user}, :ok
			# render json: {
			# 	messages: "Successfully Signed up",
			# 	is_success: true,
			# 	data: {
			# 		user: user
			# 	}
			# }, status: :ok
		else
			json_response "Something Went wrong", false, {}, :unprocessable_entity
			# render json: {
			# 	messages: "Something Went wrong",
			# 	is_success: false,
			# 	data: {}
			# },status: :unprocessable_entity
		end
	end

	private

	def user_params
		params.require(:user).permit(:email, :password, :password_confirmation)
	end

	def ensure_params_exist
		return if params[:user].present?
		 json_response "Missing params", false, {}, :bad_request
	# 	render json: {
	# 		messages: "Missing params",
	# 		is_success: false,
	# 		data: {}
	# 	}, status: :bad_request
	end
end